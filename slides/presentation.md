## Docker

#### и

### Shopify

<small>Created by [Pipa Stanislav](pipa.stanislav@gmail.com) / [@pipasv](https://t.me/pipasv)</small>

---

 - Проект
 - Dockerfile     <!-- .element: class="fragment" data-fragment-index="1" -->
 - docker-compose <!-- .element: class="fragment" data-fragment-index="2" -->
 - Что получилось <!-- .element: class="fragment" data-fragment-index="3" -->


---

### О проекте

 - UI (React)                  <!-- .element: class="fragment" data-fragment-index="1" -->
 - API (Node)                  <!-- .element: class="fragment" data-fragment-index="2" -->
 - Shopify app                 <!-- .element: class="fragment" data-fragment-index="3" -->
 - Mobile app (IOS и Android)  <!-- .element: class="fragment" data-fragment-index="4" -->
 - CI                          <!-- .element: class="fragment" data-fragment-index="5" -->

---

### Монолитное приложение

![difficult](images/difficult.jpg)

---

### Docker

![logo](images/docker-official.svg)

---

### Трудность 1

 * Плохая документация у Shopify 
 
![difficult](images/bad-documentation.jpeg) 

--

### Трудность 2

 * Dockerfile для API
   
 ```
    RUN apk add --no-cache libcurl python alpine-sdk curl-dev \
        && npm install node-libcurl --build-from-source \
        && npm install \
        && npm run build
    RUN  echo "require('node-libcurl')"|node
 ``` 

--

### Трудность 2

 * Dockerfile для UI 
   
 ```
    RUN apk add --no-cache util-linux \ 
        && yarn install \
        && yarn build
 ```
---

### Результат

 * Dockerfile для UI
   
 ```
   FROM node:10.15.0-alpine AS builderReact
   WORKDIR /app
   
   COPY package*.json ./
   COPY jsconfig.json ./
   COPY .browserslistrc ./
   COPY .postcssrc ./
   COPY /src ./src
   
   RUN apk add --no-cache util-linux \
       && yarn install \
       && yarn build
   
   FROM node:alpine AS release
   
   COPY --from=builderReact /app/build ./build
   COPY --from=builderReact /app/package.json ./
   RUN yarn add http-server
   
   EXPOSE 1234
   
   CMD [ "yarn", "prod" ]
 ```

--

### Результат

  * Dockerfile для UI
    
  ```
    FROM node:10.15.0-alpine
    WORKDIR /app
    
    COPY package*.json ./
    COPY src ./src
    COPY .env/.env.example ./.env/
    
    RUN apk add --no-cache libcurl python alpine-sdk curl-dev \
        && npm install node-libcurl --build-from-source \
        && npm install \
        && npm run build
    RUN  echo "require('node-libcurl')"|node
    
    EXPOSE 9090
    # Указываем какие команды выполнить
    CMD [ "npm", "run", "start:production" ]
  ``` 

---

### Docker-compose

![logo](images/docker-official.svg)

---

### Трудности

 - Особых не было <!-- .element: class="fragment" data-fragment-index="1" -->
 
---

### Результат

```
version: '3'

services:
  postgres:
    image: postgres:latest
    env_file:
      - .env/production/.env.postgreSQL
    volumes:
      - postgres:/data/posgres
    ports:
      - "5555:5432"
    networks:
      - appNetwork
    restart: always

  pgadmin:
    image: dpage/pgadmin4
    env_file:
    - .env/production/.env.pgAdmin
    volumes:
      - pgadmin:/root/.pgadmin
    ports:
      - "8888:80"
    networks:
      - appNetwork
    restart: always

  app-ui:
    build:
      context: ./ui
      dockerfile: Dockerfile
    image: app-ui
    ports:
      - "1234:1234"
    networks:
      - appNetwork
    restart: always

  app-api:
    build:
      context: ./
      dockerfile: Dockerfile
    image: app-api
    env_file:
    - .env/production/.env
    ports:
      - "9090:9090"
    restart: always
    networks:
    - appNetwork
    depends_on:
    - postgres
    - app-ui


networks:
  appNetwork:
    driver: bridge

volumes:
  postgres:
  pgadmin:

```

---

### Что в итоге у меня вышло

 - Приложение готовые к удаленному развертыванию <!-- .element: class="fragment" data-fragment-index="1" -->
 - Запуск проекта одной командой                 <!-- .element: class="fragment" data-fragment-index="2" -->
 
---

## Выводы
